# -*- coding: utf-8 -*-
""""""
import click
import logging
from pathlib import Path
import pandas as pd
import numpy as np
import glob
from tqdm import tqdm

tqdm.pandas()


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def main(input_filepath, output_filepath):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../interim).
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')

    target_data = load_target(input_filepath)


    for date in [
        "20220831",
        "20220901",
        "20220902",
        "20220903",
        "20220904",
        "20220905",
        "20220906",
        "20220907"
    ]:
        data = df_from_json(date, target_data, input_filepath)
        fill_nulls(
            data
        ).to_parquet(
            f"{output_filepath}/wcp_dump_{date}.parquet"
        )


def df_from_json(date, target, input_path):
    json_files = sorted(glob.glob(f"{input_path}/wcp_classifier_dump/0831_0907/{date}*.json"))
    cols = pd.merge(pd.read_json(json_files[0]), target, on='anum').columns
    cnt = 0
    df = pd.DataFrame(columns=cols)
    for i in tqdm(range(len(json_files))):
        merged = pd.merge(pd.read_json(json_files[i]), target, on='anum')
        cnt += merged.shape[0]
        df = pd.concat([df, merged])
    print(f"User number at {date} is {cnt}:")
    return df


def load_target(input_path):
    target = pd.read_csv(f"{input_path}/caller_info_hashed-2022-09-12.csv", sep=";")
    return (
        target[target.source != "automoderation"][["msisdn_hash", "category"]].rename(
            columns={"msisdn_hash": "anum", "category": "target"}
        ).dropna(

        )
    )


def fill_nulls(df):
    df = df.progress_applymap(
        lambda x: np.NaN if x == "" else x
    )
    # заполним нулями длительность звонков без ответа (call_duration) и пустую длительность ожидания (setup_duration)
    df.call_duration = df.call_duration.fillna(0.0)
    df.setup_duration = df.setup_duration.fillna(0.0)
    # удалим звонки с пустыми incoming_network
    df = df.dropna(subset="incoming_network")
    df["incoming_network_short"] = df.incoming_network.str.split(
        "-"
    ).progress_apply(
        lambda x: x[0]
    ).str.split(
        "_"
    ).progress_apply(
        lambda x: x[0]
    )
    return df


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    # load_dotenv(find_dotenv())

    main()
