# -*- coding: utf-8 -*-
""""""
import logging
from pathlib import Path

import click
import numpy as np
import pandas as pd
from sklearn.preprocessing import PolynomialFeatures
from tqdm import tqdm
from src.data.make_dataset import load_target


tqdm.pandas()


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('input_target_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
@click.argument('save', type=bool)
def main(input_filepath, input_target_filepath, output_filepath, save):
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info('making data set from raw data')
    cols = ["anum",
            "call_duration",
            "setup_duration",
            "setup_time",
            "screening_indicator",
            "location_number",
            "redirecting_party_id",
            "NET_TYPE"]
    target_data = load_target(input_target_filepath)
    for date in tqdm([
        "20220831",
        # "20220901",
        # "20220902",
        # "20220903",
        # "20220904",
        # "20220905",
        # "20220906",
        # "20220907"
    ]):
        logger.info(f'read data with date: {date}')
        data = pd.read_parquet(f"{input_filepath}/wcp_dump_{date}.parquet")[cols]
        logger.info('feature generation')
        data = feature_gen(data)
        data_grouped_by_hour = features_by_hours(data)
        poly_data = poly_features(data)
        data = pd.concat([data, poly_data], axis=1)
        for col in tqdm(
                [
                    "screening_indicator_USER_PROVIDED_VERIFIED_FAILED",
                    "screening_indicator_USER_PROVIDED_NOT_VERIFIED"
                ]
        ):
            try:
                data = data.drop(col, axis=1)
            except:
                print(f"Col {col} don't exist in table")
                pass
        data_aggregated = feature_agg(data)
        data_aggregated = pd.merge(data_aggregated, data_grouped_by_hour)

        data_aggregated = pd.merge(data_aggregated, target_data)
        data_aggregated.target = data_aggregated.target.progress_apply(
            lambda x: 1 if x == "есть жалобы на СПАМ" or x == "массовый обзвон" else 0
        )
        data_aggregated.columns = list(
            map(
                lambda x: "_".join(x) if type(x) == tuple else x, data_aggregated.columns
            )
        )
        logger.info(f'save data with date: {date}')
        if save:
            data_aggregated.to_parquet(f'{output_filepath}/df_features_{date}.parquet')
        else:
            print(data_aggregated.head(30))


def feature_gen(df):
    df = pd.get_dummies(
        df, columns=[
            "screening_indicator",
            "NET_TYPE"
        ]
    )
    df['hour'] = df.setup_time.dt.hour
    df.drop(columns=["setup_time"], inplace=True)

    df.location_number = df.location_number.progress_apply(lambda x: 1 if x else 0)
    df.redirecting_party_id = df.redirecting_party_id.progress_apply(lambda x: 1 if x else 0)

    df["setup_duration_log"] = (df.setup_duration + 1).transform(np.log)
    df["call_duration_log"] = (df.call_duration + 1).transform(np.log)
    df["setup_duration_sqrt"] = df.setup_duration.transform(np.sqrt)
    df["call_duration_sqrt"] = df.call_duration.transform(np.sqrt)
    df['sum_call'] = df.call_duration + df.setup_duration
    df['mul_call'] = df.call_duration * df.setup_duration
    df["sum_call_log"] = (df.sum_call + 1).transform(np.log)
    df["mul_call_log"] = (df.mul_call + 1).transform(np.log)
    df = df[df.sum_call_log >= 0]
    return df.reset_index(drop=True)


def mode_max(series):
    return max(pd.Series.mode(series))


def feature_agg(df):
    df_grouped = df.groupby(
        'anum'
    )["call_duration"].count(
    ).to_frame(
    )

    df_grouped = df_grouped[(df_grouped.call_duration >= 10)]

    df_more_10_calls = df[df.anum.isin(df_grouped.index)]

    funcs = ['std', 'var', 'median', 'min', 'max', 'mean', 'sum', mode_max]

    for col in tqdm(df.columns[1:]):
        df_agg = df_more_10_calls.groupby(
                'anum'
            ).agg(
                {col: funcs}
            ).fillna(
                0
            )
        df_grouped = pd.concat([df_grouped, df_agg], axis=1)
    return df_grouped.reset_index()


def poly_features(df, n=4):
    poly = PolynomialFeatures(n, interaction_only=False, include_bias=False)
    poly_features = pd.DataFrame(poly.fit_transform(
        df[[
            "call_duration",
            "setup_duration",
            "sum_call",
            "mul_call"]])).reset_index(drop=True)
    poly_features.columns = ["poly_" + str(x) for x in poly_features.columns]
    return poly_features.iloc[:, 2*n:]


def features_by_hours(df):
    hour_cols = ['anum', 'hour_0', 'hour_1', 'hour_2', 'hour_3', 'hour_4', 'hour_5',
                 'hour_6', 'hour_7', 'hour_8', 'hour_9', 'hour_10', 'hour_11', 'hour_12',
                 'hour_13', 'hour_14', 'hour_15', 'hour_16', 'hour_17', 'hour_18',
                 'hour_19', 'hour_20', 'hour_21', 'hour_22', 'hour_23']
    hour_count = pd.get_dummies(
        df[['anum', 'hour']], columns=['hour']
    ).groupby(
        'anum'
    ).sum(
    ).reset_index(
    )
    hour_count[hour_count.columns.symmetric_difference(hour_cols)] = 0
    return hour_count[hour_cols]


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    main()
