import logging

import pandas as pd
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
import click
from tqdm.notebook import tqdm_notebook
from src.utils import *
tqdm_notebook.pandas()


@click.command()
@click.argument("input_data_path", type=click.Path(exists=True))
@click.argument("n_iter", type=int)
@click.argument("best", type=bool)
# @click.argument("output_model_path", type=click.Path())
# @click.argument("output_train_metrics_path", type=click.Path())
def train_model(
        input_data_path: str,
        n_iter: int,
        best: bool
        # output_model_path: str, output_train_metrics_path: str
):

    train_dates = [
        "20220831",
        "20220901",
        "20220902",
        "20220903",
        "20220904",
        "20220905",
        # "20220906",
        # "20220907"
    ]
    val_dates = [
        # "20220831",
        # "20220901",
        # "20220902",
        # "20220903",
        # "20220904",
        # "20220905",
        "20220906",
        "20220907"
    ]
    data_train = make_dataset(input_data_path, train_dates)
    data_val = make_dataset(input_data_path, val_dates)

    cols = pd.read_csv("data/interim/columns.csv", header=None).values[:, 0]

    X_train, y_train = data_train[cols], data_train.iloc[:, -1]
    X_val, y_val = data_val[cols], data_val.iloc[:, -1]

    cv = StratifiedKFold(shuffle=True, random_state=42)

    cat = CatBoostClassifier(iterations=1000,
                             loss_function='Logloss',
                             eval_metric='TotalF1', od_type='Iter',
                             verbose=True)

    # params = {'depth': [3, 1, 2, 6, 4, 5, 7, 8, 9, 10],
    #           'iterations': [250, 100, 500, 1000],
    #           'learning_rate': [0.03, 0.001, 0.01, 0.1, 0.2, 0.3, 0.5, 0.75, 1.0],
    #           'l2_leaf_reg': [3, 1, 2, 5, 10, 50],
    #           'border_count': [32, 20, 50, 100, 200, 350, 500],
    depth = [6, 5, 7]
    iterations = [500]
    learning_rate = [0.05, 0.1, 0.15, 0.2]
    l2_leaf_reg = [3, 1, 2, 4]
    border_count = [100, 200, 500]
    #           }
    params = {'depth': [6, 7],
              'iterations': [500],
              'learning_rate': [0.05, 0.1],
              'l2_leaf_reg': [3, 2],
              'border_count': [200, 500],
              }
    gs_model = GridSearchCatboost(cat, params, scoring="roc_auc", n_iter=n_iter, cv=cv)
    gs_model.fit(X_train, y_train)

    log_results(gs_model,
                'cat',
                'catboost',
                log_only_best=best,
                est_type="catboost",
                tags={},
                X_val=X_val,
                y_val=y_val
                )


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    train_model()
