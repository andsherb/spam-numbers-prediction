import logging

import pandas as pd
from sklearn.model_selection import StratifiedKFold, RandomizedSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from tqdm.notebook import tqdm_notebook
import click
from tqdm import tqdm
from src.utils import *
tqdm_notebook.pandas()


@click.command()
@click.argument("input_data_path", type=click.Path(exists=True))
@click.argument("n_iter", type=int)
@click.argument("best", type=bool)
# @click.argument("output_model_path", type=click.Path())
# @click.argument("output_train_metrics_path", type=click.Path())
def train_model(
        input_data_path: str,
        n_iter: int,
        best: bool
        # output_model_path: str, output_train_metrics_path: str
):

    train_dates = [
        # "20220801",
        "20220802",
        "20220803",
        "20220831",
        "20220901",
        "20220902",
        "20220903",
        "20220904",
        "20220905",
        "20220906",
        # "20220907"
    ]
    val_dates = [
        # "20220801",
        # "20220802",
        # "20220803",
        # "20220831",
        # "20220901",
        # "20220902",
        # "20220903",
        # "20220904",
        # "20220905",
        # "20220906",
        "20220907"
    ]
    data_train = make_dataset(input_data_path, train_dates)
    data_val = make_dataset(input_data_path, val_dates)
    cols = pd.read_csv("data/interim/columns.csv", header=None).values[:, 0]

    X_train, y_train = data_train[cols], data_train.iloc[:, -1]
    X_val, y_val = data_val[cols], data_val.iloc[:, -1]

    cv = StratifiedKFold(shuffle=True, random_state=42)

    rfc = RandomForestClassifier(random_state=42)
    clf_rfc = Pipeline([
        ('scaler', StandardScaler()),
        ('rand_forest', rfc)])
    # Number of trees in random forest
    n_estimators = [int(x) for x in np.linspace(start=100, stop=300, num=3)]
    # Number of features to consider at every split
    max_features = ['auto', 'sqrt']
    # Maximum number of levels in tree
    max_depth = [int(x) for x in np.linspace(30, 60, num=4)]
    max_depth.append(None)
    # Minimum number of samples required to split a node
    min_samples_split = [2]
    # Minimum number of samples required at each leaf node
    min_samples_leaf = [1, 2]
    # Method of selecting samples for training each tree
    bootstrap = [True]

    params = {
        "rand_forest__n_estimators": [500],
        "rand_forest__max_depth": [30],
        # "rand_forest__max_features": max_features,
        "rand_forest__min_samples_split": [2],
        "rand_forest__min_samples_leaf": [1],
        "rand_forest__bootstrap": bootstrap,
    }

    gs_model = RandomizedSearchCV(estimator=clf_rfc, param_distributions=params,
                                  n_iter=n_iter,
                                  cv=cv,
                                  verbose=10,
                                  n_jobs=-1,
                                  scoring='roc_auc')
    gs_model.fit(X_train, y_train)

    log_results(gs_model,
                'rfc',
                'random forest',
                est_type='sklearn',
                log_only_best=best,
                tags={"rand_forest": "26.09", "n_features": "115"},
                X_val=X_val,
                y_val=y_val
                )


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    train_model()
