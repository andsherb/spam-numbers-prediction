import numpy as np
import pandas as pd
from sklearn.model_selection import ParameterSampler, KFold, GridSearchCV
from tqdm import tqdm

from sklearn.metrics import check_scoring
from sklearn.base import clone

import mlflow
import warnings
import tempfile
import os
from datetime import datetime
from catboost import Pool, CatBoostClassifier


class GridSearchCatboost(GridSearchCV):
    def __init__(
            self,
            estimator: CatBoostClassifier,
            param_grid,
            *,
            scoring=None,
            n_iter=10,
            n_jobs=None,
            refit=True,
            cv=KFold(),
            verbose=0,
            pre_dispatch="2*n_jobs",
            error_score=np.nan,
            return_train_score=False,
    ):
        super().__init__(
            estimator=estimator,
            param_grid=param_grid,
            scoring=scoring,
            n_jobs=n_jobs,
            refit=refit,
            cv=cv,
            verbose=verbose,
            pre_dispatch=pre_dispatch,
            error_score=error_score,
            return_train_score=return_train_score,
        )
        self.n_iter = n_iter
        if scoring:
            self.scoring = check_scoring(self.estimator, scoring=scoring)
        else:
            self.scoring = check_scoring(self.estimator, scoring='accuracy')
        self.cv_results_ = {'params': [], 'mean_test_score': [], 'std_test_score': [], 'index': [], 'estimator': []}
        self.best_score_ = 0,
        self.best_index_ = 0
        self.best_params_ = {}
        self.best_estimator_ = None

    def score(self, X, y=None):
        return self.scoring(self.best_estimator_, X, y)

    def fit(self, X, y=None, *, groups=None, **fit_params):
        grid = ParameterSampler(self.param_grid, n_iter=self.n_iter, random_state=42)
        for i, settings in enumerate(tqdm(grid)):
            scores = []
            base_estimator = clone(self.estimator)
            base_estimator.set_params(**settings)
            for train_index, test_index in self.cv.split(X, y):
                X_train, X_test = X.iloc[train_index], X.iloc[test_index]
                y_train, y_test = y.iloc[train_index], y.iloc[test_index]
                train_pool = Pool(data=pd.DataFrame(X_train), label=y_train)
                valid_pool = Pool(data=pd.DataFrame(X_test), label=y_test)

                base_estimator.fit(
                    train_pool,
                    eval_set=valid_pool,
                    plot=True
                )
                score = self.scoring(base_estimator, valid_pool, y_test)
                scores.append(score)
            avg_score = np.mean(scores)
            std_score = np.std(scores)
            self.cv_results_['params'].append(settings)
            self.cv_results_['mean_test_score'].append(avg_score)
            self.cv_results_['std_test_score'].append(std_score)
            self.cv_results_['index'].append(i)
            self.cv_results_['estimator'].append(base_estimator)
            if avg_score > self.best_score_:
                self.best_score_ = avg_score
                self.best_params_ = settings
                self.best_estimator_ = base_estimator
                self.best_index_ = i
        return self


def log_run_catboost(gridsearch, model_name: str, run_index: int, conda_env: dict,
                     tags=None, X_val=None, y_val=None):
    """Logging of cross validation results to mlflow tracking server

    Args:
        gridsearch (GridSearchCV): fitted Gridsearch
        model_name (str): Name of the model
        run_index (int): Index of the run (in Gridsearch)
        conda_env (dict): A dictionary that describes the conda environment (MLFlow Format)
        tags (dict): Dictionary of extra data and tags (usually features)
        val_data (Dataframe): Validation data
    """

    if tags is None:
        tags = {}
    cv_results = gridsearch.cv_results_
    # print(cv_results)
    with mlflow.start_run(run_name=str(run_index)) as run:

        mlflow.log_param("folds", gridsearch.cv)

        print("Logging parameters")

        try:
            params = list(gridsearch.param_grid.keys())
        except AttributeError:
            params = list(gridsearch.param_distributions.keys())

        for param in params:
            mlflow.log_param(param, cv_results['params'][run_index][param])
        mlflow.log_param("metric", gridsearch.scoring)

        print("Logging metrics")
        for score_name in [score for score in cv_results if "mean_test" in score]:
            # print(score_name)
            mlflow.log_metric(score_name, cv_results[score_name][run_index])
            mlflow.log_metric(score_name.replace("mean", "std"),
                              cv_results[score_name.replace("mean", "std")][run_index])
        if X_val is not None and y_val is not None:
            val_metric = gridsearch.scoring(cv_results['estimator'][run_index],
                                            X_val,
                                            y_val)
            mlflow.log_metric("val_score", val_metric)

        print("Logging model")
        mlflow.catboost.log_model(gridsearch.best_estimator_, model_name, conda_env=conda_env)

        print("Logging CV results matrix")
        tempdir = tempfile.TemporaryDirectory().name
        os.mkdir(tempdir)
        timestamp = datetime.now().isoformat().split(".")[0].replace(":", ".")
        filename = "%s-%s-cv_results.csv" % (model_name, timestamp)
        csv = os.path.join(tempdir, filename)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            pd.DataFrame(cv_results).to_csv(csv, index=False)

        mlflow.log_artifact(csv, "cv_results")

        print("Logging extra data related to the experiment")
        mlflow.set_tags(tags)

        run_id = run.info.run_uuid
        experiment_id = run.info.experiment_id
        print(mlflow.get_artifact_uri())
        print("runID: %s" % run_id)
        mlflow.end_run()


def log_run_sklearn(gridsearch, model_name: str, run_index: int, conda_env: dict,
                    tags=None, X_val=None, y_val=None):
    """Logging of cross validation results to mlflow tracking server

    Args:
        model_name (str): Name of the model
        run_index (int): Index of the run (in Gridsearch)
        conda_env (str): A dictionary that describes the conda environment (MLFlow Format)
        tags (dict): Dictionary of extra data and tags (usually features)
        val_data (Dataframe): Validation data

    """

    cv_results = gridsearch.cv_results_
    # print(cv_results)
    with mlflow.start_run(run_name=str(run_index)) as run:

        mlflow.log_param("folds", gridsearch.cv)

        print("Logging parameters")

        try:
            params = list(gridsearch.param_grid.keys())
        except AttributeError:
            params = list(gridsearch.param_distributions.keys())

        for param in params:
            mlflow.log_param(param, cv_results["param_%s" % param][run_index])
            print("param_%s" % param)

        print("Logging metrics")
        for score_name in [score for score in cv_results if "mean_test" in score]:
            # print(score_name)
            mlflow.log_metric(score_name, cv_results[score_name][run_index])
            mlflow.log_metric(score_name.replace("mean", "std"),
                              cv_results[score_name.replace("mean", "std")][run_index])
        if X_val is not None and y_val is not None:
            val_metric = gridsearch.score(X_val,
                                          y_val)
            mlflow.log_metric("val_score", val_metric)

        print("Logging model")
        mlflow.sklearn.log_model(gridsearch, model_name, conda_env=conda_env)

        print("Logging CV results matrix")
        tempdir = tempfile.TemporaryDirectory().name
        os.mkdir(tempdir)
        timestamp = datetime.now().isoformat().split(".")[0].replace(":", ".")
        filename = "%s-%s-cv_results.csv" % (model_name, timestamp)
        csv = os.path.join(tempdir, filename)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            pd.DataFrame(cv_results).to_csv(csv, index=False)

        mlflow.log_artifact(csv, "cv_results")

        print("Logging extra data related to the experiment")
        mlflow.set_tags(tags)

        run_id = run.info.run_uuid
        experiment_id = run.info.experiment_id
        print(mlflow.get_artifact_uri())
        print("runID: %s" % run_id)
        mlflow.end_run()


def log_results(gridsearch, experiment_name, model_name, est_type,
                tags=None, log_only_best=False, X_val=None, y_val=None):
    """Logging of cross validation results to mlflow tracking server

    Args:
        experiment_name (str): experiment name
        model_name (str): Name of the model
        est_type (str): Type of estimator: catboost or sklearn
        tags (dict): Dictionary of extra tags
        log_only_best (bool): Whether to log only the best model in the gridsearch or all the other models as well
        val_data (Dataframe): Validation data

    """
    conda_env = {
        'name': 'mlflow-env',
        'channels': ['defaults'],
        'dependencies': [
            'python=3.8.0',
            'scikit-learn>=0.21.3',
            {'pip': ['xgboost==1.0.1']}
        ]
    }

    best = gridsearch.best_index_

    mlflow.set_tracking_uri("http://0.0.0.0:5000")
    mlflow.set_experiment(experiment_name)

    if est_type == "catboost":
        log_run = log_run_catboost
    else:
        log_run = log_run_sklearn

    if log_only_best:
        log_run(gridsearch, model_name, best, conda_env, tags, X_val, y_val)
    else:
        for i in range(len(gridsearch.cv_results_['params'])):
            log_run(gridsearch, model_name, i, conda_env, tags, X_val, y_val)


def make_dataset(data_path: str, dates: list) -> pd.DataFrame:
    data = pd.read_parquet(f"{data_path}/df_features_{dates[0]}.parquet")
    dataset = data.copy()
    for date in tqdm(dates[1:]):
        data = pd.read_parquet(f"{data_path}/df_features_{date}.parquet")
        dataset = pd.concat([dataset, data])
    return dataset
